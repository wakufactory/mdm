<?php
$bdir = "data" ;
$bid = $_GET['bid'] ;
$d = opendir($bdir) ;
$n = array() ;
$bids = array() ;
while($f = readdir($d)) {
	if($f=="."||$f=="..") continue ;
	if(preg_match("/^([^-]+)-(.+)\.md$/",$f,$a)) {
		$bids[$a[1]]++ ;
		if($bid!="" && $bid!=$a[1]) continue ;
		$t = mktoc($f) ;
		$n[] = array("bid"=>$a[1],"name"=>$a[2],'mtime'=>date("Y-m-d H:i:s",filemtime("data/".$f)),
		'size'=>filesize("data/".$f),'toc'=>$t ) ;
	}
}

$sc = $_GET['s'] ;
function sf($a,$b){
	global $sc ;
	switch($sc) {
		case "N":
			$r = ($a['name']>$b['name'])?1:-1 ;
			break;
		default:
			$r = ($a['mtime']>$b['mtime'])?-1:1 ;
	}
	return $r ;
} ;

usort($n,sf) ;
//echo "<pre>" ;print_r($n) ;

function mktoc($f) {
	global $bdir ;
	$tcc = array(1,1,1,1,1,1) ;
	$m  = file($bdir."/".$f) ;
	$t = array() ;
	for($i=0;$i<count($m);$i++)  {
		$l = $m[$i] ;
		if(preg_match("/^([\#]{1,6})(.+)$/",$l,$a)) {
			$lv = strlen($a[1]); 
			$tt = $a[2] ;
			if(preg_match("/^(.+){(.+)}$/",$a[2],$aa)) $tt = $aa[1] ;
			$t[] = array('l'=>$lv,'id'=>"h$lv-".$tcc[$lv]++,'t'=>$tt) ;
		}
	}
	return $t ;
}
