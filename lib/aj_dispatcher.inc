<?php
# A class implementing an event driven web programming paradigm
# in php

function array2xml($ar,$ph="",&$mm="") {
	if(!is_array($ar)) return ;
	$j = array() ;
	$m = "" ;
	foreach( $ar as $k=>$v ) {
		if($m==="") { if($k=="0") $m = 0 ; else $m = 1 ; }
			$mmm = "" ;
		if(is_array($v)) {
			$v = array2xml($v,$k,$mmm) ;
		} else {
			if(!is_int($v)) $v = mb_convert_encoding(htmlspecialchars(str_replace("\r","",str_replace("\n","\\n",str_replace("\0","",$v)))),"UTF-8",mb_internal_encoding()) ;
		}
		if($m===0)  {
			$j[] = "<$ph>".$v."\n</$ph>" ;
		} else {
			if($mmm===0) $j[] = $v ; else $j[] = "<$k>".$v."</$k>" ;
		}
	}
	$mm = $m ;
	if($m==0) return join("\n",$j) ;
	else return join("\n",$j) ;
}

class aj_dispatcher {
  # The name of the GET or POST variable which
  # holds the event name
  var $_event_var;
	var $_jsonp_var ;
	var $_outmode_var ;

  # Must not ever call these methods as an event
  var $_bad_method_names;

  var $SESSION ;
  var $GET,$POST ;

  function aj_dispatcher(){
    # Set vars to default values:
    # can be overridden by child class
    $this->_event_var='method';
    $this->_jsonp_var='jsonp';
    $this->_outmode_var='format';
    $this->_bad_method_names = array(
      'dispatcher' => TRUE,
      'dispatch' => TRUE,
      '_isa_valid_method' => TRUE,
      '_session' => TRUE,
      '_session_set' => TRUE
    );
    $this->_bad_method_names[get_class($this)] = TRUE;
  }

  function _session() {
    @session_start() ;
    $this->SESSION = $_SESSION ;
  }
	function _session_set($k,$v) {
		$_SESSION[$k] = $v ;
		$this->SESSION[$k] = $v ;
	}

  function dispatch($mode=""){
    # Determine which event to call: first look in CGI GET
    # variables, then in POST variables, and if not found
    # call the 'main' event
    if (
      array_key_exists($this->_event_var,$_GET) &&
      $_GET[$this->_event_var] != '' &&
      $this->_isa_valid_method("On_".$_GET[$this->_event_var])
    ){
      $event = "On_".$_GET[$this->_event_var];
      $this->GET = arrayDecode($_GET) ;
     $this->POST = arrayDecode($_POST) ;
    } 
    elseif (
      array_key_exists($this->_event_var,$_POST) &&
      $_POST[$this->_event_var] != '' &&
      $this->_isa_valid_method("On_".$_POST[$this->_event_var])
    ){
      $event = "On_".$_POST[$this->_event_var];
      $this->POST = arrayDecode($_POST) ;
    } else {
     $event = 'On_default';
     $this->GET = arrayDecode($_GET) ;
     $this->POST = arrayDecode($_POST) ;
    }
    # Invoke event; Make method call
    $this->_session() ;
    $ret = $this->$event(array('get'=>$this->GET,'post'=>$this->POST));
		if(array_key_exists($this->_outmode_var,$_GET)) {
			$mode = $_GET[$this->_outmode_var] ;
		}
		if($mode=="xml") {
			header("Content-type: application/xml; charset=utf-8") ;
			echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" ;
    	echo array2xml($ret) ;
		} else 
		if(array_key_exists($this->_jsonp_var,$_GET )) {
			$jsonp=$_GET[$this->_jsonp_var] ;
			if($jsonp=="") $jsonp="callback" ;
    	echo array2jsonp($ret,$jsonp) ;
		} else if($mode=="dump") {
			echo "<pre>" ;
			echo print_r($ret) ;
		} else if($mode=="php") {
			echo serialize($ret) ;
		} else {
    	echo array2json($ret) ;
		}
  }

  function _isa_valid_method($method){
    if (array_key_exists($method,$this->_bad_method_names)){
      return FALSE;
    } elseif (
      method_exists($this, $method)
    ){
      return TRUE;
    } else {
      return FALSE;
    }
  }

}
//--------------------------------------------------------------------------
// functions for post/get
//   version 1.3.2  2006/08/14
//   by wakufactory.jp
//   license BSD

//配列を再帰的にデコード
function arrayDecode($array) {
	foreach($array as $k=>$v ) {
		if( is_array($v) ) $array[$k] = arrayDecode($v) ;
//		else $array[$k] = str_replace("'","\'",stripslashes(uniDecode($v))) ;
//		else $array[$k] = stripslashes(uniDecode($v)) ;
		else $array[$k] = uniDecode($v) ;
	}
	return $array ;
}
//utf-8のデコード
function uniDecode($str,$charcode=""){
	if($charcode=="") $charcode=mb_internal_encoding() ;
	$text = preg_replace_callback("/%u[0-9A-Za-z]{4}/",toUtf8,$str);
	return mb_convert_encoding($text, $charcode, 'UTF-8');
}
function toUtf8($ar){
	foreach($ar as $val){
		$val = intval(substr($val,2),16);
		if($val < 0x7F){        // 0000-007F
			$c .= chr($val);
		}elseif($val < 0x800) { // 0080-0800
			$c .= chr(0xC0 | ($val / 64));
			$c .= chr(0x80 | ($val % 64));
		}else{                // 0800-FFFF
			$c .= chr(0xE0 | (($val / 64) / 64));
			$c .= chr(0x80 | (($val / 64) % 64));
			$c .= chr(0x80 | ($val % 64));
		}
	}
	return $c;
}

//配列->JSON変換
function array2json($ar) {
	if(!is_array($ar)) return ;
	$j = array() ;
	$m = "" ;
	foreach( $ar as $k=>$v ) {
		if($m==="") { if($k=="0") $m = 0 ; else $m = 1 ; }
		if(is_array($v)) $v = array2json($v) ;
		else {
			if(!is_int($v)) $v = "\"".str_replace("","",str_replace("/","\/",str_replace('"','\"',(str_replace("\n","\\n",$v)))))."\"" ;
		}
		if($m==0) $j[] = $v ;
		else $j[] = "\"".$k."\":".$v ;
	}
	if($m==0) return "[".join(",",$j)."]" ;
	else return "{".join(",",$j)."}" ;
}
//jsonp
function array2jsonp($arg,$func="callback") {
  $s =  "$func(\n" ;
	$s .= array2json($arg) ;
	$s .= ");\n" ;
	return $s ;
}
//hashのみとりだす。
function hashonly($r) {
	for($i=0;$i<count($r);$i++) {
		foreach( $r[$i] as $k=>$v ) {
			if(!is_numeric($k)) $q[$i][$k] = $v ;
		}
	}
	return $q ;
}

?>
