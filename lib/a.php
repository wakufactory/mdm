<?php
require_once "aj_dispatcher.inc" ;
//require_once "htmltemplate.inc" ;

class page extends aj_dispatcher {
	var $basedir = "../"; 
	var $datadir = "data/" ;
	var $imgdir = "img/" ;
	var $filedir = "files/" ;
	var $expdir = "export/" ;
	
	var $toc = array() ;

	function getid($id) {
		if(preg_match("/^([^-]+)-(.+)/",$id,$a)) {
			return $a ;
		}
	}
	function on_default() {
		return array('status'=>-1) ;
	}
	
	function on_save($pa) {
		$p = $pa['get'] ;
		$id = $p['id'];
		$sid = $this->getid($id) ;
		unset($p['id']) ;
		unset($p['method']);
		if($pa['post']) {
			$p = $pa['post'] ;
			$fp = fopen($this->basedir.$this->datadir."$id","w" ) ;
			fputs($fp,serialize($p)) ;
			fclose($fp) ;
			$fp = fopen($this->basedir.$this->datadir."$id.md","w" ) ;
			fputs($fp,str_replace("\\n","\n",$p['t'])) ;
			fclose($fp) ;
		}
	
		header("Access-Control-Allow-Origin:*") ;
		$st = 0 ;
		
		if($_FILES) {
			$f = $_FILES['f'] ;
			if($f['error']!=0) $st = -1 ;
			else {
				if(preg_match("/image/",$f['type'])) $d = $this->imgdir ;
				else $d = $this->filedir ;
				copy($f['tmp_name'],$this->basedir.$d.$sid[0]."-".$f['name']) ;
			}
			
		}
		return array('status'=>$st ) ;
	}
	function on_load($p) {
		$id = $p['get']['id'] ;
		if(($r = $this->_loaddata($id))==null) {
			return array('status'=>-1) ;
		}
		header("Access-Control-Allow-Origin:*") ;
		return array('status'=>0,'data'=>$r) ;
	}
	function on_loadpv($p) {
		$id = explode("/",$p['get']['id']) ;
		if(($r = $this->_loadpv($id[0],$id[1]))==null) {
			return array('status'=>-1) ;
		}
		//pass2 
		$t = $r['t'] ;
		$nt = array() ;
		for($i=0;$i<count($t);$i++) {
			if(preg_match("/%T (.+)/",$t[$i],$a)) {
				$tl = array() ;
				for($j=0;$j<count($this->toc);$j++) {
					if($this->toc[$j]['level']==1) {
						$u = urlencode($this->toc[$j]['text']) ;
						$nt[] = " 1. [".$this->toc[$j]['text']."](#i".$u.")\n" ;
					}
				}
			} else $nt[] = $t[$i] ;
		}
		$r['t'] = $nt ;
		header("Access-Control-Allow-Origin:*") ;
		return array('status'=>0,'data'=>$r,'toc'=>$this->toc) ;
	}
	function _loadpv($bid,$id) {
		$data = $this->_loaddata($bid."-".$id) ;
		if($data==null) return null ;
		$r = array() ;
		$b = $data['t'] ;
		for($i=0;$i<count($b);$i++) {
			if(preg_match("/^%I ([^ ]+)/",trim($b[$i]),$a)) {
				$dd = $this->_loadpv($bid,$a[1]) ;
				if($dd!=null) {
					$dl = $dd['t'] ;
					$r = array_merge($r,$dl) ;
				}
				continue ;
			}
			if(preg_match("/^(#{1,6})(.+)$/",$b[$i],$a)) {
				$tt = $a[2] ;
				if(preg_match("/^(.+) {(.+)}$/",$a[2],$aa)) $tt = $aa[1] ;
				$this->toc[] = array("text"=>$tt,"level"=>strlen($a[1]) ) ;
			}
			$r[] = $b[$i] ;
		}
		$data['t'] = $r ;
		return $data ;
	}
	function _loaddata($id) {
		$fn = $this->basedir.$this->datadir."$id" ;
		if(file_exists($fn.".md")) {
			return array('t'=>file($fn.".md"),'dt'=>date('Y-m-d H:i:s',filemtime($fn.".md"))) ;
		} else {
			$fp = @fopen($this->basedir.$this->datadir."$id","r");
			if(!$fp) {
				return null ;
			}
			return  unserialize(fgets($fp)) ;
		}		
	}
	function on_export($p) {
		$id = $p['get']['id'] ;
		$d = $this->_loaddata($id) ;
		$res = $this->_getres($d['t']) ;
		$h = $p['post']['h'] ;
		$a = array('body'=>$h) ;
		if($res['h'][0]['l']==1) $a['title'] = $res['h'][0]['t'] ;
		$t = new HtmlTemplate()	;
		$expdir = $this->basedir.$this->expdir ;
		system("rm -r ".$expdir) ;
		@mkdir($expdir."css") ;
		@mkdir($expdir."img") ;
		$page = $t->t_buffer("../mde_temp.html",$a) ;
		$sf = fopen($expdir."index.html","w") ;
		fputs($sf,$page) ;
		fclose($sf) ;
		copy("../css/bootstrap.min.css",$expdir."css/bootstrap.min.css") ;
		copy("../css/userstyle.css",$expdir."css/userstyle.css") ;
		for($i=0;$i<count($res['imgs']);$i++) {
			copy($this->basedir.$this->imgdir.$res['imgs'][$i],$expdir."img/".$res['imgs'][$i]) ;
		}
		chdir($this->basedir.$this->expdir) ;
		system("zip -r page.zip * >/dev/null") ;
		
		return array('status'=>0,'zip'=>$this->expdir."page.zip");
	}
	function _getres($md) {
		$r = array() ;
		if(preg_match_all("/!\[([^\]]+)\]\(([^\)]+)/",$md,$a)) {
			$r['imgs'] = $a[1] ;
		}
		if(preg_match_all("/([#]{1,6})([^\\\\]+)/",$md,$a)) {
			$h = array() ;
			for($i=0;$i<count($a[1]);$i++) {
				$h[] = array('l'=>strlen($a[1][$i]),'t'=>$a[2][$i]) ;
			}
			$r['h'] = $h ;
		}
		return $r ;
	}
}

$o = new page ;
$o->dispatch() ;