jQuery.fn.extend({
	togglelist:function() {
		this.on('click','ul,ol,dl',function(e) {
			var o = $(this).offset() ;
			if((e.pageY - o.top )>40 ) return ;	
			$(this).toggleClass("c").children().toggle();
		});
		
		this.on('click','li,dt,dd',function(e) {
			return false ;
		});		
	}
})