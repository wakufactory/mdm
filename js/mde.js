var mde = {
bid:null,
id:null,
img_path:"img/",
file_path:"files/",
init:function(bid,id ) {
	mde.bid = bid ;
	mde.id = id ;
	
	mde.load(mde.id,function(d) {
		$('#edit textarea').val(d) ;
		$('#target').html(mde.marked(d)) ;
		location.hash = location.hash.substr(1)+" " ;
		$('#edit').show() ;
		$('#target').addClass("editing") ;
		$('#bedit').hide();
	});

	$('#bedit').on('click',function() {
		$('#edit').show() ;
		$('#target').addClass("editing") ;
		$(this).hide();

	})
	$('#beend').on('click',function() {
		$('#edit').hide() ;
		$('#target').removeClass("editing") ;
		$('#bedit').show() ;
	})
	$('#bexp').on('click',function() {
		mde.export(mde.id,$('#target').html()) ;
	})
	$('#edit textarea').on('change',function(e) {
		$('#target').html(mde.marked($(this).val())) ;
		mde.save(mde.id,$(this).val());
	})	
	$('#edit textarea').on('keyup',function(e) {
		if(e.keyCode!=0x0d && e.keyCode!=0x08) return ;
		$('#target').html(mde.marked($(this).val())) ;
		mde.save(mde.id,$(this).val());
	})	
	$('#edit').on('drop',function(e){
		$('#edit').removeClass('focus') ;
		var tmp = (e.originalEvent.dataTransfer.files) ;
		if(!tmp.length) return ;

		var files = Array.prototype.slice.call(tmp, 0, tmp.length);
		mde.upload(files,function(f) {
			var name = f.name ;
			if(name.normalize) name =  name.normalize('NFC') ;
			if(f.type.match(/image/)) {
				insertAtCaret($('#edit textarea'),"!["+name+"]("+mde.img_path+mde.bid+"-"+mde.id+"-"+name+")\n") ;
			} else {
				insertAtCaret($('#edit textarea'),'['+name+"]("+mde.file_path+mde.bid+"-"+mde.id+"-"+name+' "*")\n');
			}
			mde.update() ;
		}) ;
		return false ;
	}).on('dragenter',function(e) {
		$('#edit').addClass('focus') ;
		return false ;
	}).on('dragover',function(e) {
		return false ;
	}).on('dragleave',function(e) {
		$('#edit').removeClass('focus') ;
		return false ;
	});
	
	MD.init();
}
,
update:function() {
	var text = $('#edit textarea').val();
	$('#target').html(mde.marked(text)) ;
	mde.save(mde.id,text);	
}
,
upload:function(files,cb) {
	files.forEach(function(f){
		var reader = new FileReader() ;
		reader.onload = function(e) {
			$('#msg').html("uploading "+f.name) ;
			var fd = new FormData() ;
			fd.append("f",f) ;
			$.ajax({
				url:"lib/a.php?method=save&id="+mde.bid+"-"+mde.id,
                async: true,
                xhr : function(){
                            XHR = $.ajaxSettings.xhr();
                            XHR.upload.addEventListener('progress',function(e){
	                                     console.log(e) ;
                                     })
	                        return XHR;
	                       },
				method:"POST",
				data:fd,
				processData: false,
				contentType: false
			}).done(function(d){
				$('#msg').html("upload complete") ;
				cb(f) ;
			}).error(function(e) {
				alert("save error" ) ;	
			});					
			
		}
		reader.readAsArrayBuffer(f) ;
	})
}
,
save:function(id,data) {
	var dt = new Date() ;
	var fd = new FormData() ;
	fd.append("dt",dt.toLocaleString()) ;
	fd.append("t",data.replace(/\\/g,"\\\\").replace(/\n/g,"\\n")) ;

	$.ajax({
		url:"lib/a.php?method=save&id="+mde.bid+"-"+id,
		method:"POST",
		data:fd,
		processData: false,
		contentType: false
	}).done(function(d){
//		console.log(d) ;
		$('#msg').html("saved") ;
	}).error(function(e) {
		alert("save error" ) ;	
	});		
}
,
load:function(id,cb) {
	$.getJSON("lib/a.php?method=load&id="+mde.bid+"-"+id,function(d) {
		var data = d.data.t.join("") ;
		if(data) cb(data) ;
	});
}
,
export:function(id,data) {
	var fd = new FormData() ;
	fd.append("h",data) ;

	$.ajax({
		url:"lib/a.php?method=export&id="+mde.bid+"-"+id,
		method:"POST",
		data:fd,
		processData: false,
		contentType: false
	}).done(function(d){
		console.log(d);
		d = JSON.parse(d) ;
		$('#msg').html('exported <a href="'+d.zip+'">download zip</a>') ;
	}).error(function(e) {
		alert("export error" ) ;	
	});	
}
,
marked:function(str) {
	return MD.render(str) ;
}	

}
function insertAtCaret(target, str) {
	var obj = $(target);
	obj.focus();
	var s = obj.val();
	var p = obj.get(0).selectionStart;
	var np = p + str.length;
	obj.val(s.substr(0, p) + str + s.substr(p));
	obj.get(0).setSelectionRange(np, np);
}