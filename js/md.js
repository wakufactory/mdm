var MD = {
	renderer:null,
	init:function() {
		MD.renderer = new marked.Renderer();
		MD.renderer.heading = function (text, level) {
			var attr = null ;
			if(text.match(/(.+) {(.+)}$/)) {
				text = RegExp.$1 ;
				attr = MD._getattr(RegExp.$2) ;
			}
			var escapedText = encodeURIComponent(text);

			var sect = "" ;
			if(this.lastsect >= level) {
				for(var i=0;i<this.lastsect-level+1;i++) sect += "</section>" ;
			}
			this.lastsect = level ;
			var ats = "" ;
			if(attr) {
				ats = 'class="SH'+level+' '+attr.cls.join(" ")+'" ' ;
				for(var at in attr.attr) {
					ats += "data-"+at+'="'+attr.attr[at]+'" ' ;
				}
			} else ats = 'class="SH'+level+'"' ;
			var ret = '<h' + level + '>'+ text + '</h' + level + '>';
			ret =  sect+'<section id="i'+escapedText+'" '+ats+'>' + ret ;
			return ret ;
		} ;
	
		MD.renderer.image = function(href, title, text) {
			var cls  ;
			var attr = null ;
			if(text.match(/(.+){(.+)}$/)) {
				text = RegExp.$1 ;
				attr = MD._getattr(RegExp.$2) ;
			}
			var ats = "" ;
			if(attr) {
				if(attr.cls) ats = 'class="'+attr.cls.join(" ")+'" ' ;
				for(var at in attr.attr) {
					ats += "data-"+at+'="'+attr.attr[at]+'" ' ;
				}
			} 
			var out = '<img src="' + href + '" alt="' + text + '" ';
			if (title) {
				out += ' title="' + title + '" ';
			}
			out += ats + (this.options.xhtml ? ' />' : ' >') ;
			if(title) {
				out = "<figure>"+out+"<figcaption>"+title+"</figcaption></figure>" ;
			}
			return out;
		};
	},
	render:function(str) {
		return marked(str,{gfm:true,renderer:MD.renderer}) ;
	},
	_getattr:function(s) {
		var l = s.split(" ") ;
		var at = {} ;
		var cls = [] ;
		for(var i in l) {
			if(l[i].match(/\.(.+)/)) cls.push( RegExp.$1) ;
			else if(l[i].match(/(.+)=(.+)/)) at[RegExp.$1] = RegExp.$2 ;
		}
		return {cls:cls,attr:at} ;
	}
}